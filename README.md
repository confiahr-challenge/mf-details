<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>

## 🚀 Instrucciones



1. **Iniciar MF**

    ```
    npm install
    npm run develop
    ```

    Localhost: http://localhost:8001

    Internet: https://main--aquamarine-tulumba-a58c46.netlify.app/
