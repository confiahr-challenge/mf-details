export interface ILangMap {
  es: string;
  en: string;
}
