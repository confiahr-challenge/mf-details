import { ICharacter } from './ICharacter';
export interface IMovie {
  id: number;
  title: {
    es: string;
    en: string;
  };
  characters: ICharacter[];
  cover?: string;
  img?: string;
}
