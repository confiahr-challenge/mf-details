import { ILangMap } from './ILangMap';

export interface ICharacter {
  image: string;
  name: ILangMap;
  actor: string;
}
