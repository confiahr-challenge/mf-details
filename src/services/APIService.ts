import { getMovieDetails } from './movies/getMovieDetails';

export const APIService = {
  movies: {
    getDetails: getMovieDetails,
  },
};
