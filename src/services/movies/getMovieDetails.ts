import { IMovie } from '../../interfaces/IMovie';
import movies from './../../data/movies.json';

export const getMovieDetails = (id: number): Promise<IMovie> => {
  const movie = (movies.find((item) => item.id == id) || {}) as IMovie;
  const result = Promise.resolve(movie);
  return result;
};
