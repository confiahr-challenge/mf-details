import React from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import MovieDetails from './MovieDetails/MovieDetails';
import { getLangFromLs } from '../utils/localstorage';

const queryClient = new QueryClient();

interface Props {
  id: number;
}

const App: React.FC<Props> = (props) => {
  const lang = getLangFromLs();
  return (
    <QueryClientProvider client={queryClient}>
      <MovieDetails id={parseInt(String(props.id))} lang={lang} />
    </QueryClientProvider>
  );
};

export default App;
