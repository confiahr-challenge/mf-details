import React from 'react';
import Avatar from '../Avatar/Avatar';
import { Wrapper, Text } from './styles/CharacterItemStyle';
import {ICharacter} from "../../interfaces/ICharacter";
import {ILangCode} from "../../interfaces/ILangCode";

interface Props {
  character: ICharacter;
  lang: ILangCode;
}

const CharacterItem: React.FC<Props> = ({ character, lang }) => {
  return (
    <Wrapper>
      <Avatar src={character.image} />
      <Text $isBold>{character.name[lang]}</Text>
      <Text>{character.actor}</Text>
    </Wrapper>
  );
};

export default CharacterItem;
