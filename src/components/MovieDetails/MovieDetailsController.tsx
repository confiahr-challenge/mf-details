import { useQuery } from '@tanstack/react-query';
import { APIService } from '../../services/APIService';

export const useMovieDetailsController = (id: number) => {
  const req = useQuery({
    queryKey: [`GET_MOVIE_${id}`],
    queryFn: () => APIService.movies.getDetails(id),
  });

  const movie = req.data;
  const hasCharacters = !!movie?.characters?.length;

  return { movie, hasCharacters };
};
