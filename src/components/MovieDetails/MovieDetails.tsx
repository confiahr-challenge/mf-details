import React from 'react';
import CharacterList from './CharacterList';
import { ILangCode } from '../../interfaces/ILangCode';
import { useMovieDetailsController } from './MovieDetailsController';

interface Props {
  id: number;
  lang: ILangCode;
}

const MovieDetails: React.FC<Props> = ({ lang, id }) => {
  const { movie, hasCharacters } = useMovieDetailsController(id);
  return (
    <>
      {movie && hasCharacters && (
        <CharacterList characters={movie.characters} lang={lang} />
      )}
    </>
  );
};

export default MovieDetails;
