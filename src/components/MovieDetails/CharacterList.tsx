import React from 'react';
import CharacterItem from './CharacterItem';
import { Wrapper } from './styles/CharacterListStyle';
import { ICharacter } from '../../interfaces/ICharacter';
import { ILangCode } from '../../interfaces/ILangCode';

interface Props {
  characters: ICharacter[];
  lang: ILangCode;
}
const CharacterList: React.FC<Props> = ({ characters, lang }) => {
  return (
    <Wrapper>
      {characters.map((item) => (
        <CharacterItem key={item.image} character={item} lang={lang} />
      ))}
    </Wrapper>
  );
};

export default CharacterList;
