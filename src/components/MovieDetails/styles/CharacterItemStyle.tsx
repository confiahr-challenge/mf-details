import styled from 'styled-components';

export const Wrapper = styled.div`
  flex: 0 0 calc(50% - 0.5rem);
  padding: 2rem;
  border: 1px solid #eeeeee;
  box-shadow: 0px 3px 5px #eeeeee;
  border-radius: 10px;
  box-sizing: border-box;
  place-items: center;
  flex-direction: column;
  display: flex;
  gap: 1rem
`;

interface TextProps {
  $isBold?: boolean;
}
export const Text = styled.p<TextProps>`
  text-align: center;
  color: #656565;
  font-weight: ${({ $isBold }) => ($isBold ? '400' : '200')};
`;
