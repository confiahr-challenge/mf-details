import styled from 'styled-components';

export const AvatarWrapper = styled.div`
  width: 100px;
  height: 100px;
  overflow: hidden;
  border-radius: 50%;
`;

export const Image = styled.img`
  width: 100%;
  object-fit: cover;
  height: 100%;
  display: block;
`;

