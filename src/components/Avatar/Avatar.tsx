import React from 'react';
import { AvatarWrapper, Image } from './AvatarStyles';

interface Props {
  src: string;
}
const Avatar: React.FC<Props> = ({ src }) => {
  return (
    <AvatarWrapper>
      <Image src={src} loading="lazy" />
    </AvatarWrapper>
  );
};

export default Avatar;
